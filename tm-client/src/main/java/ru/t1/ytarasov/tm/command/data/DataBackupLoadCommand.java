package ru.t1.ytarasov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.dto.request.data.DataBackupLoadRequest;

public final class DataBackupLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-backup-load";

    @NotNull
    public static final String DESCRIPTION = "Loads from backup";

    @Override
    @SneakyThrows
    public void execute() {
        getDomainEndpoint().loadDataBackup(new DataBackupLoadRequest(getToken()));
    }

    @Nullable
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }
}
