package ru.t1.ytarasov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.enumerated.Role;
import ru.t1.ytarasov.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    @NotNull
    User create(@NotNull String login, @NotNull String passwordHash);

    @NotNull
    User create(@NotNull String login, @NotNull String passwordHash, @NotNull String email);

    @NotNull
    User create(@NotNull String login, @NotNull String passwordHash, @NotNull Role role);

    @Nullable
    User findByLogin(@NotNull String login);

    @Nullable
    User findByEmail(@NotNull String email);

    boolean isLoginExists(@NotNull String login);

    boolean isEmailExists(@NotNull String email);

}
