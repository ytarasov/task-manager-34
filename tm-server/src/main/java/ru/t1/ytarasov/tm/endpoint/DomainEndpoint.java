package ru.t1.ytarasov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.ytarasov.tm.api.endpoint.IDomainEndpoint;
import ru.t1.ytarasov.tm.api.service.IDomainService;
import ru.t1.ytarasov.tm.api.service.IServiceLocator;
import ru.t1.ytarasov.tm.dto.request.data.*;
import ru.t1.ytarasov.tm.dto.response.data.*;
import ru.t1.ytarasov.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.ytarasov.tm.api.endpoint.IDomainEndpoint")
public final class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    @NotNull
    private final IDomainService domainService;

    public DomainEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
        this.domainService = serviceLocator.getDomainService();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public DataBackupLoadResponse loadDataBackup(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBackupLoadRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataBackup();
        return new DataBackupLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public DataBackupSaveResponse saveDataBackup(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBackupSaveRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataBackup();
        return new DataBackupSaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public DataBase64LoadResponse loadDataBase64(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBase64LoadRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataBase64();
        return new DataBase64LoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public DataBase64SaveResponse saveDataBase64(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBase64SaveRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataBase64();
        return new DataBase64SaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public DataBinaryLoadResponse loadDataBinary(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBinaryLoadRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataBinary();
        return new DataBinaryLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public DataBinarySaveResponse saveDataBinary(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBinarySaveRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataBinary();
        return new DataBinarySaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public DataJsonFasterXmlLoadResponse loadDataJsonFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataJsonFasterXmlLoadRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataJsonFasterXml();
        return new DataJsonFasterXmlLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public DataJsonFasterXmlSaveResponse saveDataJsonFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataJsonFasterXmlSaveRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataJsonFasterXml();
        return new DataJsonFasterXmlSaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public DataJsonJaxbLoadResponse loadDataJsonJaxb(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataJsonJaxbLoadRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataJsonJaxb();
        return new DataJsonJaxbLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public DataJsonJaxbSaveResponse saveDataJsonJaxb(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataJsonJaxbSaveRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataJsonJaxb();
        return new DataJsonJaxbSaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public DataXmlFasterXmlLoadResponse loadDataXmlFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataXmlFasterXmlLoadRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataXmlFasterXml();
        return new DataXmlFasterXmlLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public DataXmlFasterXmlSaveResponse saveDataXmlFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataXmlFasterXmlSaveRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataXmlFasterXml();
        return new DataXmlFasterXmlSaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public DataXmlJaxbLoadResponse loadDataXmlJaxb(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataXmlJaxbLoadRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataXmlJaxb();
        return new DataXmlJaxbLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public DataXmlJaxbSaveResponse saveDataXmlJaxb(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataXmlJaxbSaveRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataXmlJaxb();
        return new DataXmlJaxbSaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public DataYamlLoadResponse loadDataYaml(@NotNull DataYamlLoadRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataYaml();
        return new DataYamlLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public DataYamlSaveResponse saveDataYaml(@NotNull DataYamlSaveRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataYaml();
        return new DataYamlSaveResponse();
    }

}
