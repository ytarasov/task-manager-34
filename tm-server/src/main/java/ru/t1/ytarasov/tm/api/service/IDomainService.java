package ru.t1.ytarasov.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

public interface IDomainService {

    @SneakyThrows
    void loadDataBackup();

    @SneakyThrows
    void saveDataBackup();

    @SneakyThrows
    void loadDataBase64();

    @SneakyThrows
    void saveDataBase64();

    @SneakyThrows
    void loadDataBinary();

    @SneakyThrows
    void saveDataBinary();

    @SneakyThrows
    void loadDataJsonFasterXml();

    @SneakyThrows
    void saveDataJsonFasterXml();

    @SneakyThrows
    void loadDataJsonJaxb();

    @SneakyThrows
    void saveDataJsonJaxb();

    @NotNull
    @SneakyThrows
    void loadDataXmlFasterXml();

    @SneakyThrows
    void saveDataXmlFasterXml();

    @SneakyThrows
    void loadDataXmlJaxb();

    @SneakyThrows
    void saveDataXmlJaxb();

    @SneakyThrows
    void loadDataYaml();

    @SneakyThrows
    void saveDataYaml();

}
