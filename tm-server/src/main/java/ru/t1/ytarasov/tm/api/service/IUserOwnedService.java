package ru.t1.ytarasov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.enumerated.Sort;
import ru.t1.ytarasov.tm.exception.AbstractException;
import ru.t1.ytarasov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IService<M> {

    void clear(@Nullable String userId) throws AbstractException;

    boolean existsById(@Nullable String userId, @Nullable String id) throws AbstractException;

    @Nullable
    List<M> findAll(@Nullable String userId) throws AbstractException;

    @Nullable
    List<M> findAll(@Nullable String userId, @Nullable Comparator comparator) throws AbstractException;

    @Nullable
    List<M> findAll(@Nullable String userId, @Nullable Sort sort) throws AbstractException;

    @Nullable
    M findOneById(@Nullable String userId, @Nullable String id) throws AbstractException;

    @Nullable
    M findOneByIndex(@Nullable String userId, int index) throws AbstractException;

    int getSize(@Nullable String userId) throws AbstractException;

    @Nullable
    M removeById(@Nullable String userId, @Nullable String id) throws AbstractException;

    @Nullable
    M removeByIndex(@Nullable String userId, int index) throws AbstractException;

    @Nullable
    M add(@Nullable String userId, @Nullable M model) throws AbstractException;

    @Nullable
    M remove(@Nullable String userId, @Nullable M model) throws AbstractException;

}
