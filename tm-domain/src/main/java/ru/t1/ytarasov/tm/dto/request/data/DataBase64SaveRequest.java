package ru.t1.ytarasov.tm.dto.request.data;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class DataBase64SaveRequest extends AbstractDataRequest {

    public DataBase64SaveRequest(@Nullable String token) {
        super(token);
    }

}
