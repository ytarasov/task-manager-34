package ru.t1.ytarasov.tm.dto.request.data;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class DataXmlFasterXmlLoadRequest extends AbstractDataRequest {

    public DataXmlFasterXmlLoadRequest(@Nullable String token) {
        super(token);
    }

}
