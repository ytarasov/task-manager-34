package ru.t1.ytarasov.tm.dto.request.data;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class DataJsonJaxbSaveRequest extends AbstractDataRequest {

    public DataJsonJaxbSaveRequest(@Nullable String token) {
        super(token);
    }

}
